/**
* Herramientas de extensión de la clase String clásica de lenguajes 
* como lo es Visual C# adaptado a JavaScript para Node.js.
* Fecha: 23-09-2020
* Autor: Ariel Alejandro Wagner
*/
class StringTools {
    constructor() {

    }

    /**
     * Comprueba si el valor está vacío o es nulo. Retorna true si 
     * es nulo o vacío y retorna false si no lo es.
     * @param {*} value Valor a probar.
     */
    static isNullOrEmpty(value) 
    {
        return (value === null || value === "" || value == "") ? true : false;
    }

    /**
     * Comprueba si el valor es un espacio en blanco o es nulo. Retorna true si 
     * es nulo o espacio en blanco y retorna false si no lo es.
     * @param {*} value Valor a probar.
     */    
    static isNullOrWhiteSpace(value) 
    {
        return (value === null || /^\s*$/.test(value)) ? true : false;
    }
}

module.exports = {
    StringTools
}